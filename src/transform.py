import argparse
import os


def sort_and_write_set(file, iterable):
    if iterable:
        file.write(" ".join(map(str, sorted(iterable)))+"\n")
        iterable.clear()


def sort_and_write_seq(file, iterable_a, iterable_b):
    _str = ""
    for k in iterable_a.keys():
        l_a = iterable_a.get(k, [])
        l_b = iterable_b.get(k, [])
        if l_a:
            item = " ".join((map(str, sorted(l_a))))
            _str += item + " -1 "
        if l_b:
            item = " ".join((map(str, sorted(l_b))))
            _str += item + " -1 "
    file.write(_str+"-2"+"\n")
    iterable_a.clear()
    iterable_b.clear()


def get_feature_number(dictionary, _str):
    feature_number = dictionary.get(_str[1:])
    if feature_number is None:
        feature_number = len(dictionary) + 1
        dictionary[_str[1:]] = feature_number
    return feature_number


def do_seq(in_file):
    name_to_number = dict()
    transaction_id = 0
    player_a = dict()
    player_b = dict()
    a_then_b = True
    with open(in_file, "r") as f_r, open(in_file+"_tmp", "w") as f_w_tmp:
        for line in f_r:
            line = line.rstrip()
            line = line.split()
            _id = int(line[0])
            if _id != transaction_id:
                if a_then_b:
                    sort_and_write_seq(f_w_tmp, player_a, player_b)
                else:
                    sort_and_write_seq(f_w_tmp, player_b, player_a)

            else:
                card = line[1]
                turn = line[2]
                if card[0] == "M":
                    if len(player_a) == 0 and len(player_b) == 0:
                        a_then_b = True
                    if player_a.get(turn) is None:
                        player_a[turn] = set()
                    player_a[turn].add(get_feature_number(name_to_number, card))

                elif card[0] == "O":
                    if len(player_a) == 0 and len(player_b) == 0:
                        player_a = dict()
                        player_b = dict()
                        a_then_b = False
                    if player_b.get(turn) is None:
                        player_b[turn] = set()
                    player_b[turn].add(get_feature_number(name_to_number, card))
            transaction_id = _id
    return name_to_number


def do_set(in_file):
    name_to_number = dict()
    transaction_id = 0
    player_a = set()
    player_b = set()
    with open(in_file, "r") as f_r, open(in_file+"_tmp", "w") as f_w_tmp:
        for line in f_r:
            line = line.rstrip()
            line = line.split()
            _id = int(line[0])
            if _id != transaction_id:
                sort_and_write_set(f_w_tmp, player_a)
                sort_and_write_set(f_w_tmp, player_b)
            else:
                card = line[1]
                if card[0] == "M":
                    player_a.add(get_feature_number(name_to_number, card))
                elif card[0] == "O":
                    player_b.add(get_feature_number(name_to_number, card))
            transaction_id = _id
    return name_to_number


def create_itmesets(in_file, out_file, seq):
    print("{} to {}".format(in_file, out_file))
    if seq != 0:
        name_to_number = do_seq(in_file)
    else:
        name_to_number = do_set(in_file)
    with open(in_file+"_tmp", "r") as f_r, open(out_file, "w") as f_w:
        f_w.write("@CONVERTED_FROM_TEXT\n")
        for key, value in name_to_number.items():
            f_w.write("@ITEM={}={}".format(str(value), key)+"\n")
        for line in f_r:
            f_w.write(line)
        os.remove(in_file+"_tmp")


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-seq', action='store',type=int, dest='seq', help='0 for set parsing, 1 for seq parsing')
    parser.add_argument('-in', action='store', dest='in_file', help='input file name')
    parser.add_argument('-out', action='store', dest='out_file', help='output file name')

    results = parser.parse_args()
    create_itmesets(results.in_file, results.out_file, results.seq)
