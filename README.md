Data mining heartstone
-----------------------------------------------------------------------------------------

What is it?
------------

This projet is an assignment for the IA course at Université de Rennes 1.
It consists of data transformation in order to find closed frequent card sets,
closed frequent sequences of card and sequential rules from Hearthstone games.

We used the SPMF implementation of the following algorithms:
* LCM for closed frequent itemsets 
* CloSpan for closed frequent sequences 
* RuleGrowth for sequential rules 

How to use it?
---------------

For transformation: 

run: python src/transform.py -seq [0|1] -in input_file -out output_file

- seq 1: transform the data for CloSpan and RuleGrowth algorithms
- seq 0: tranform the data for LCM algorithm

For SPMF usage:

run: java -jar src/spmf.jar

Then follow the doc for the data mining algorithm you want:

* LCM: http://www.philippe-fournier-viger.com/spmf/LCM.php
* CloSpan: http://www.philippe-fournier-viger.com/spmf/CloSpan.php
* RuleGrowth: http://www.philippe-fournier-viger.com/spmf/RuleGrowth.php

(overleaf share link: https://www.overleaf.com/project/5c94ce4d648bc01d0987215f)